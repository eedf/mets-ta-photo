FROM php:8.2-apache

# Debian packages
RUN apt-get update -y
RUN apt-get install -y locales unzip libicu-dev libzip-dev zip libpng-dev logrotate git libjpeg-dev

# Locale
RUN echo "fr_FR.utf8" > /etc/locale.gen
RUN locale-gen

# PHP extensions
RUN docker-php-ext-configure gd --with-jpeg
RUN docker-php-ext-install pdo_mysql intl zip gd

# Apache modules
RUN a2enmod rewrite && a2enmod status

# Apache config
COPY vhost.conf /etc/apache2/sites-available/000-default.conf

# PHP config
RUN mv "$PHP_INI_DIR/php.ini-production" "$PHP_INI_DIR/php.ini"
COPY php.ini /usr/local/etc/php/conf.d/roads.ini

# Code
COPY ./ ./

# Composer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
RUN composer install

# Dir temp export
RUN mkdir -p public/uploads/photos && chown www-data: public/uploads/photos

